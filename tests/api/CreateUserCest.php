<?php

use yii\helpers\Url;

class CreateUserCest
{
    protected function registerUrl()
    {
        return Url::to(['/site/register']);
    }

    public function registerCorrectUser(ApiTester $I)
    {
        $I->sendPOST($this->registerUrl(), $this->correctUserPost());

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 1, 'message' => '']);
    }

    public function registerDuplicateUser(ApiTester $I)
    {
        $I->sendPOST($this->registerUrl(), $this->correctUserPost());

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 1, 'message' => '']);

        $I->sendPOST($this->registerUrl(), $this->correctUserPost());

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 0, 'message' => 'User name "user1" has already been taken.']);

        $I->sendPOST($this->registerUrl(), $this->correctUser2WithDuplicateEmailWithUser1Post());
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 0, 'message' => 'Email "user1@user1.com" has already been taken.']);
    }

    public function registerNotCorrectUser(ApiTester $I)
    {
        $I->sendPOST($this->registerUrl(), $this->incorrectUsernamePost());

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 0, 'message' => 'User name should contain at least 4 characters.']);

        $I->sendPOST($this->registerUrl(), $this->incorrectArUsernamePost());

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 0, 'message' => 'User name يجب أن يحتوي على أكثر من ٤ حروف.']);
    }

    public function registerUserWithShortPassword(ApiTester $I)
    {
        $I->sendPOST($this->registerUrl(), $this->shortPasswordPost());

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 0]);

        $I->sendPOST($this->registerUrl(), $this->shortPasswordArPost());

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 0]);
    }

    public function _after()
    {
        $user = \app\models\User::findOne(['username' => $this->correctUserPost()['username']]);
        if (!is_null($user)) {
            $user->delete();
        }
    }

    protected function correctUserPost()
    {
        return [
            'username' => 'user1',
            'password' => 'user1_password',
            'email' => 'user1@user1.com',
            'subscribe' => true,
            'token_ios' => Yii::$app->security->generateRandomString(40),
            'lang' => 'en',
        ];
    }

    protected function correctUser2WithDuplicateEmailWithUser1Post()
    {
        return [
            'username' => 'user2',
            'password' => 'user2_password',
            'email' => 'user1@user1.com',
            'subscribe' => true,
            'token_ios' => Yii::$app->security->generateRandomString(40),
            'lang' => 'en',
        ];
    }

    protected function incorrectUsernamePost()
    {
        return [
            'username' => 'as',
            'password' => 'user1_password',
            'email' => 'user1@user1.com',
            'subscribe' => true,
            'token_ios' => Yii::$app->security->generateRandomString(40),
            'lang' => 'en',
        ];
    }

    protected function incorrectArUsernamePost()
    {
        $post = $this->incorrectUsernamePost();
        $post['lang'] = 'ar';
        return $post;
    }

    protected function shortPasswordPost()
    {
        return [
            'username' => 'user1',
            'password' => 'pass',
            'email' => 'user1@user1.com',
            'subscribe' => true,
            'token_ios' => Yii::$app->security->generateRandomString(40),
            'lang' => 'en',
        ];
    }

    protected function shortPasswordArPost()
    {
        $post = $this->shortPasswordPost();
        $post['lang'] = 'ar';
        return $post;
    }

}