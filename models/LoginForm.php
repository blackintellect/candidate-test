<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $token_ios;
    public $token_android;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError($attribute, 'Incorrect username or password.');
                return;
            }

            $interval = (time() - strtotime($user->last_password_attempt));

            if ($user->password_attempt_count >= 5 && $interval < 30 * 60) {
                $time = round(30 - $interval / 60);
                $time !== 0 ? : $time++;
                $error = 'You have been blocked for 30 minutes due to failing to log in multiple times. Possibly someone tried to hack your account. Please wait ' . $time . ' minutes.';
                $this->addError($attribute, $error);
                return;
            } else if ($user->password_attempt_count >= 5 && $interval >= 30 * 60) {
                $user->password_attempt_count = 0;
                $user->save();
            }

            if (!Yii::$app->security->validatePassword($this->password, $user->password)) {
                $user->updateAttributes(['last_password_attempt' => date("Y-m-d H:i:s"), 'password_attempt_count' => $user->password_attempt_count + 1]);
                $error = 'Incorrect username or password. For security reasons, you will be blocked for 30 minutes after five unsuccessful attempts. Attempts count: ' . $user->password_attempt_count;
                $this->addError($attribute, $error);
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        $user = $this->getUser();
        $user->token_android = $this->token_android;
        $user->token_ios = $this->token_ios;

        if ($this->validate()) {
            $user->password_attempt_count = 0;
            $user->save();
            return Yii::$app->user->login($user);
        }

        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
