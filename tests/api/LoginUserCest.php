<?php


use app\models\User;
use yii\helpers\Url;

class LoginUserCest
{
    /** @var ApiTester */
    protected $I;

    protected function registerUrl()
    {
        return Url::to(['/site/register']);
    }

    protected function loginUrl()
    {
        return Url::to(['/site/login']);
    }

    protected function registerUser($post)
    {
        $this->I->sendPOST($this->registerUrl(), $post);
    }

    public function _before(ApiTester $I)
    {
        $this->I = $I;
    }

    public function _after()
    {
        $user = User::findOne(['username' => $this->correctUserPost()['username']]);
        if (!is_null($user)) {
            $user->delete();
        }
    }

    // tests
    public function loginCorrectUser(ApiTester $I)
    {
        $this->registerUser($this->correctUserPost());
        $I->sendPOST($this->loginUrl(), $this->correctUserPost());
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 1, 'message' => '']);
    }

    public function tryLoginIncorrectUserManyTimes(ApiTester $I)
    {
        $this->registerUser($this->correctUserPost());

        $I->sendPOST($this->loginUrl(), $this->incorrectUserPasswordPost());
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 0]);

        for ($i = 0; $i < 4; $i++) {
            $I->sendPOST($this->loginUrl(), $this->incorrectUserPasswordPost());
            $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
            $I->seeResponseIsJson();
            $I->seeResponseContainsJson(['response' => 0]);
        }

        $I->sendPOST($this->loginUrl(), $this->correctUserPost());
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['response' => 0]);
    }

    protected function correctUserPost()
    {
        return [
            'username' => 'user1',
            'password' => 'user1_password',
            'email' => 'user1@user1.com',
            'subscribe' => true,
            'token_ios' => Yii::$app->security->generateRandomString(40),
            'lang' => 'ar',
        ];
    }

    protected function incorrectUserPasswordPost()
    {
        return [
            'username' => 'user1',
            'password' => 'user2_password',
            'token_ios' => Yii::$app->security->generateRandomString(40),
            'lang' => 'ar',
        ];
    }
}
