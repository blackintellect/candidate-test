<?php

use yii\db\Migration;

class m170224_141204_user extends Migration
{
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(20)->notNull(),
            'password' => $this->string(128)->notNull(),
            'email' => $this->string(128)->notNull(),
            'subscribe' => $this->boolean()->notNull(),
            'token_ios' => $this->string(40),
            'token_android' => $this->string(40),
        ]);
    }

    public function down()
    {
        echo "m170224_141204_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
