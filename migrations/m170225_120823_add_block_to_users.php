<?php

use yii\db\Migration;

class m170225_120823_add_block_to_users extends Migration
{
    public function up()
    {
        $this->addColumn('users', 'last_password_attempt', $this->dateTime());
        $this->addColumn('users', 'password_attempt_count', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        echo "m170225_120823_add_block_to_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
