<?php

/**
 * Created by PhpStorm.
 * User: chuchu
 * Date: 24.02.2017
 * Time: 18:00
 */
class RegistrationCest
{

    public function _before(\FunctionalTester $I)
    {
        $I->amOnPage(\yii\helpers\Url::to(['/site/register']));
    }

    public function testSuccess(\FunctionalTester $I)
    {
        $I->amOnPage(\yii\helpers\Url::to(['/site/register']));

        $I->sendAjaxPostRequest(\yii\helpers\Url::to(['/site/register']), [
            'username' => 'lol',
            'password' => 'asdasd',
            'email' => 'sobaka@soba.ka',
            'subscribe' => true,
            'token_ios' => Yii::$app->security->generateRandomString(40),
            'lang' => 'en',
        ]);

        $I->seeResponseContainsJson(['response' => 1, 'message' => '']);
    }

}